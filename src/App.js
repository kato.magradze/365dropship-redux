import './App.css';
import { useEffect, useState } from 'react';
import { Switch, Route, Redirect, useLocation, useHistory } from 'react-router';
import Main from './components/Main/Main';
import Aside from './components/Aside/Aside';
import { Dashboard } from './components/Dashboard/Dashboard';
import { Inventory } from './components/Inventory/Inventory';
import { Cart } from './components/Cart/Cart';
import { Orders } from './components/Orders/Orders';
import { Transactions } from './components/Transactions/Transactions';
import { Stores } from './components/Stores/Stores';
import { Profile } from './components/Profile/Profile';
import { LandingPage } from './components/LandingPage/LandingPage';
import { Authorization } from './components/LandingPage/Authorization';
import Snackbar from './components/Snackbar/Snackbar';


function App() {

  const location = useLocation();
  const history = useHistory();
  const [renderAside, setRenderAside] = useState(false);
  const [loader, setLoader] = useState(true)

  useEffect(() => {
    if(location.pathname === "/" || location.pathname === "/register" || location.pathname === "/login") {
      setRenderAside(false);
    }
    else {
      setRenderAside(true);
    }
  }, [location, renderAside])
    
  useEffect(() => {
      const token = localStorage.getItem('token');
      if(!token && location.pathname !== "/") {
          history.push("/login");
      }
      setTimeout(() => setLoader(false), 100);
      //eslint-disable-next-line
  }, [JSON.parse(localStorage.hasOwnProperty('token')), history]);

  if(loader) {
      return null;
  }
  return (
    <div className="App">
      <Snackbar/>
      {renderAside && <Aside/>}
      <Switch>
        <Route exact path="/">
          <LandingPage/>
        </Route>
        <Route exact path="/catalog">
          <Redirect to="/catalog/all-categories"/>
        </Route>
        <Route exact path="/catalog/:category?/add">
          <Main/>
        </Route>
        <Route exact path="/catalog/:category?/:id?/:modification?">
          <Main/>
        </Route>
        <Route path="/dashboard">
          <Dashboard/>
        </Route>
        <Route path="/inventory">
          <Inventory/>
        </Route>
        <Route path="/cart/:id?">
          <Cart/>
        </Route>
        <Route path="/orders">
          <Orders/>
        </Route>
        <Route path="/transactions">
          <Transactions/>
        </Route>
        <Route path="/stores">
          <Stores/>
        </Route>
        <Route path="/profile">
          <Profile/>
        </Route>
        <Route path="/register">
          <Authorization/>
        </Route>
        <Route path="/login">
          <Authorization/>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
