const handleSearchProducts = (products, searchValue) => {
    if(searchValue) {
        const filteredSearch = products.filter(product => {
            return product.title.toLowerCase().includes(searchValue.toLowerCase());
        })
        return filteredSearch;
    }
    return products;

}

export default handleSearchProducts