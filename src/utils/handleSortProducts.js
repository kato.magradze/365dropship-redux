const handleSortProducts = (products, sortValue) => {

    if(sortValue === "asc") {
        products.sort((a,b) => a.price - b.price);
    }
    else if(sortValue === "desc") {
        products.sort((a,b) => b.price - a.price);
    }
    else if(sortValue === "alphabetic") {
        products.sort((a,b) => a.title.localeCompare(b.title));
    }
    else if(sortValue === "reverse-alphabetic") {
        products.sort((a,b) => b.title.localeCompare(a.title));
    }
    else {
        products.sort((a,b) => a.id - b.id);
    }

    return products;
}

export default handleSortProducts;