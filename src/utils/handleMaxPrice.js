const handleMaxPrice = (products) => {
    const prices = products.map(item => item.price);
    return [Math.min(...prices), Math.max(...prices)];
}

export default handleMaxPrice