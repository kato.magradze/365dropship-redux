const handlePriceRange = (products, priceRangeValue) => {
    products = products.filter(product => {
        return product.price >= priceRangeValue[0] && product.price <= priceRangeValue[1]
    })

    return products;
}

export default handlePriceRange;