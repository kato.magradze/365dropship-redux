import handlePriceRange from "./handlePriceRange";
import handleSearchProducts from "./handleSearchProducts";
import handleSortProducts from "./handleSortProducts";

const applyFilters = (products, sortValue, searchValue, priceRangeValue) => {
    let productsCopy = [...products];
    
    productsCopy = handleSortProducts(productsCopy, sortValue);
    productsCopy = handleSearchProducts(productsCopy, searchValue);
    productsCopy = handlePriceRange(productsCopy, priceRangeValue);

    return productsCopy;
}

export default applyFilters;