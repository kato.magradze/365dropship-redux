const handleProductCheck = (selectedProducts, id) => {

    const foundId = selectedProducts[id];
 
    if(foundId) {
        delete selectedProducts[id];
        return {...selectedProducts};
     }
    else {
        return({
            ...selectedProducts,
            [id]: true
        }) 
    }
}

export default handleProductCheck;