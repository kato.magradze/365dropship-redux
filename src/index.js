import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createTheme, ThemeProvider} from '@material-ui/core';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { compose, applyMiddleware, createStore } from 'redux';
import { rootReducer } from './reducers';
import thunk from 'redux-thunk';
import { SnackbarProvider } from 'notistack';
import Slide from '@material-ui/core/Slide';

const composeEnchanser = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnchanser(applyMiddleware(thunk))
);

const dropshipTheme = createTheme({
  palette: {
    primary: {
      main: '#61D5DF'
    }, 
    secondary: {
      main: '#5D6B9F'
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 740,
      md: 1500,
      lg: 1700
    }
  },
  typography: {
    fontFamily: 'Gilroy-medium'
  }
})

ReactDOM.render(
    <BrowserRouter>
      <SnackbarProvider 
        maxSnack={4}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
        }}
        style={{marginBottom: "10px"}}
        TransitionComponent={Slide}
        dense={false}
      >
        <Provider store={store}>
          <ThemeProvider theme={dropshipTheme}>
              <App />
          </ThemeProvider>
        </Provider>
      </SnackbarProvider>
    </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
