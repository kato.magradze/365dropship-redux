import axios from "axios";
import { SERVER_URL } from "./serviceConstants";

axios.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
    return config;
})

export const signUp = async (firstName, lastName, email, password, passwordConfirmation) => {
    try {
        const res = await axios.post(SERVER_URL + "register", {firstName, lastName, email, password, passwordConfirmation});
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem("token", res.data.data.token);
    }
    catch(err) {
        throw new Error(err);
    }
}

export const logIn = async (email, password) => {
    try {
        const res = await axios.post(SERVER_URL + "login", {email, password});
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem("token", res.data.data.token);
    }
    catch(err) {
        throw new Error(err);
    }
}