import axios from "axios";
import { SERVER_URL_V1, SERVER_URL } from "./serviceConstants";

export const getUser = async (id) => {
    try {
        const res = await axios.get(SERVER_URL_V1 + `users/${id}`);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    }
}

export const updateUser = async (id, firstName, lastName, email, password) => {
    try {
        const res = await axios.put(SERVER_URL_V1 + `users/${id}`, { firstName, lastName, email, password });
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    }
}

export const checkPassword = async (email, password) => {
    try {
        const res = await axios.post(SERVER_URL + "login", {email, password});
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    }
}