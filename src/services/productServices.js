import axios from "axios";
import { SERVER_URL_V1 } from "./serviceConstants";

export const getAllProductsList = async () => {
    try {
        const res = await axios.get(SERVER_URL_V1 + 'products');
        return res.data.data
    } catch (err) {
        throw new Error(err);
    }
}

export const getSingleProduct = async (id) => {
    try {
        const res = await axios.get(SERVER_URL_V1 + `products/${id}`);
        return res &&  res.data.data
    } catch (err) {
        throw new Error(err);
    }
}

export const addProduct = async (data) => {
    try {
        const res = await axios.post(SERVER_URL_V1 + `products`, data);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    } 
} 

export const editProduct = async (id, data) => {
    try {
        const res = await axios.put(SERVER_URL_V1 + `products/${id}`, data);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    } 
} 

export const removeProduct = async (id) => {
    try {
        const res = await axios.delete(SERVER_URL_V1 + `products/${id}`);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    } 
} 