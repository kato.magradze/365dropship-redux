import axios from "axios";
import { SERVER_URL_V1 } from "./serviceConstants";

export const getCart = async () => {
    try {
        const res = await axios.get(SERVER_URL_V1 + "cart");
        return res.data.data;
    } catch(err) {
            // if(err.response.status === 401) {
            //     window.location.href="/login";
            // }
            throw new Error(err);
    }
}

export const addToCart = async (productId, qty) => {
    try {
        const res = await axios.post(SERVER_URL_V1 + `cart/add`, {productId, qty});
        return res
    } catch (err) {
        console.log(err);
    }
}

export const removeFromCart = async (id) => {
    try {
        await axios.post(SERVER_URL_V1 + `cart/remove/${id}`);
    }
    catch(err) {
        console.log(err);
        throw new Error(err);
    }
}