const icons = [
    {
        name: 'Dashboard',
        path: 'dashboard',
        class: "fas fa-tachometer-alt"
    },
    {   name: 'Catalog',
        path: 'catalog/all-categories',
        class: "fas fa-list-ul"
    },
    {   name: 'Inventory',
        path: 'inventory',
        class: "fas fa-boxes"
    },
    {   name: 'Cart',
        path: 'cart',
        class: "fas fa-shopping-cart"
    },
    {   name: 'Orders',
        path: 'orders',
        class: "fas fa-clipboard-check"
    },
    {   name: 'Transactions',
        path: 'transactions',
        class: "fas fa-exchange-alt"
    },
    {   name: 'Stores',
        path: 'stores',
        class: "fas fa-clipboard-list"
    }
]

export default icons;