import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        padding: '5px 18px'
    },
    button: {
        color: "white",
        margin: "5px 10px",
        whiteSpace: 'nowrap',
        fontWeight: '600',
        flexShrink: '0'
    },
    smallButton: {
        textTransform: 'unset',
        // fontWeight: '600',
        fontFamily: "Gilroy-regular",
        fontSize: '14px'
    }, 
    mediumAndBig: {
        '@media (max-width:1366px)': {
            fontSize: '11px'
        },
        '@media (max-width: 1300px)': {
            fontSize: '10px'
        }
    },
    addProductButton: {
        maxWidth: '42px', 
        maxHeight: '42px', 
        minWidth: '42px', 
        minHeight: '42px',

        '@media (max-width:1366px)': {
            maxWidth: '35px',
            maxHeight: '35px', 
            minWidth: '35px', 
            minHeight: '35px',
        },
        '@media (max-width: 1300px)': {
            maxWidth: '33px',
            maxHeight: '33px', 
            minWidth: '33px', 
            minHeight: '33px'
        }
    }, 
    selectAllIcon: {
        display: 'none',
        maxWidth: '42px', 
        maxHeight: '42px', 
        minWidth: '33px', 
        minHeight: '33px',
        '@media (max-width: 1055px)': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
    filterIcon: {
        display: 'none',
        maxWidth: '42px', 
        maxHeight: '42px', 
        minWidth: '33px', 
        minHeight: '33px',
        '@media (max-width: 935px)': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
    selectAllButton: {
        '@media (max-width: 1055px)': {
            display: 'none'
        }
    },
    clearAllButton: {
        '@media (max-width: 1055px)': {
            display: 'none'
        }
    },
    resetFilterButton: {
        width: '90%',
        margin: '25px 12px 10px',
        '@media (max-width: 1366px)': {
            fontSize: '13px'
        },
        '@media (max-width: 1300px)': {
            fontSize: '13px'
        }
    },
    container: {
        padding: 20,
        overflowY: 'scroll',
        height: 'calc(100vh - 122px)'
    },
    sortIcon: {
        color: '#B6B9CA',
        marginRight: '5px'
    },
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: "white",
        border: '2px solid #000',
        padding: "30px"
    },
    textField: {
       '&:hover': {
        //    border: '1px solid red'
        // backgroundColor: 'red'
        // color: 'rgba($red, 0.75) !important'
        // transition: 'none !important'
       } 
    },
    removeAll: {
        backgroundColor: 'lightgrey'
    },
    helperText: {
        position: 'absolute',
        top: '3.5rem'
    },
    rowHead: {
        color: '#303856',
        fontFamily: 'Gilroy-bold',
        fontWeight: 600,
        fontSize: '14px',
        textTransform: 'uppercase',
    },
    rowTitle: {
        fontFamily: 'Helvetica, serif',
        fontSize: '17px',
        color: '#303856'
    },
    rowStats: {
        fontFamily: 'Gilroy-light',
        fontSize: '16px',
        color: '#303856'
    },
    tableCell: {
        '&:hover': { 
            backgroundColor: "rgba(0, 0, 0, 0.03)",
            cursor: 'pointer',
            transitionDuration: '0.2s'
        }
    },
    tableContainer: {
        "&::-webkit-scrollbar": {
            height: "5px",
        },
        "&::-webkit-scrollbar-track": {
            background: "transparent",
        },
        "&::-webkit-scrollbar-thumb": {
            borderRadius: "1.2px",
            border: "4px solid #dadbdd",
        },
    },
    deactivateButton: {
        color: "#ff7b7b",
        backgroundColor: "#fff1f1",
        fontWeight: '600',
        whiteSpace: 'nowrap',
        '@media (max-width: 675px)': {
            display: 'none'
        }
    },
    profileField: {
        width: '100%',
        margin: '15px 0'
    },
    burgerMenu: {
        display: 'none',
        '@media (max-width: 1070px)': {
            display: 'block',
            marginLeft: '5px',
            fontSize: '35px',
            cursor: 'pointer'
        }
    },
})

export default useStyles;