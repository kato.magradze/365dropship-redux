import React from 'react'
import './Choose.css'

const Choose = ({title, modifier, categories}) => {
    return (
        <div className={`choose choose__${modifier}`}>
            <div className={`choose__item choose__item--${modifier}`}>{title}</div>
            
                {/* {categories && 
                    <ul className={`choose__list ${menu ? 'choose__list--visible' : 'choose__list--hidden'}`}>
                        <li className={`choose-list__item ${currentCategory === "all-categories" ? "choose-list__item--clicked" : ""}`} value="all-categories" onClick={handleCategoryChange}>...</li>
                        {categories.map(category => {
                            return <li className={`choose-list__item ${currentCategory === category ? "choose-list__item--clicked" : ""}`} key={category} value={category} onClick={handleCategoryChange}>{category}</li>
                        })}
                    </ul>
                } */}
        </div>
    )
}

export default Choose
