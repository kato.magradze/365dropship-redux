import React, { useEffect } from 'react'
import './Sidebar.css'
import BlueButton from '../Catalog/Header/BlueButton'
import Choose from './Choose'
import ShipOptions from './ShipOptions'
import Range from './Range'
import { useSelector, useDispatch } from 'react-redux'
import { profitRangeFilter, priceRangeFilter, setPriceRange, setProfitRange } from '../../../reducers/ProductsReducer/ProductsActions'
import applyFilters from '../../../utils/applyFilters'

const Sidebar = () => {

    const rangeMinMax = useSelector(state => state.products.rangeMinMax);

    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const searchValue = useSelector(state => state.products.searchValue);
    const sortValue = useSelector(state => state.products.sortValue);

    const priceRange = useSelector(state => state.products.rangeStates.priceState);
    const profitRange = useSelector(state => state.products.rangeStates.profitState);

    useEffect(() => {
        dispatch(setPriceRange([rangeMinMax[0], rangeMinMax[1]]))
        dispatch(setProfitRange([0, 100]))
    }, [rangeMinMax, dispatch])

    const handleResetFilter = () => {
        rangeMinMax && dispatch(setPriceRange([rangeMinMax[0], rangeMinMax[1]]))
        dispatch(setProfitRange([0, 100]))
        const filteredPriceRange = applyFilters(products, sortValue, searchValue, [rangeMinMax[0], rangeMinMax[1]]);
        dispatch(priceRangeFilter(filteredPriceRange, [rangeMinMax[0], rangeMinMax[1]]));
        dispatch(profitRangeFilter([0, 100]));
    }

    return (
        <div className="aside__item aside__item--choose">
            <Choose title="Choose Niche" modifier="niche" categories={null}/>
            <Choose title="Choose Category" modifier="category" categories={null}/>
            <ShipOptions title="Ship From" modifier="shipfrom"/>
            <ShipOptions title="Ship To" modifier="shipto"/>
            <ShipOptions title="Select Supplier" modifier="supplier"/>
            <Range title="Price Range" id="priceRange" minValue={rangeMinMax[0]} maxValue={rangeMinMax[1]} rangeValue={priceRange} setRangeValue={setPriceRange} val="$"/>
            <Range title="Profit Range" id="profitRange" minValue={0} maxValue={100} rangeValue={profitRange} setRangeValue={setProfitRange} val="%"/>
            <BlueButton title="Reset filter" className="resetFilterButton" handleClick={handleResetFilter} medium/>
        </div>
    )
}

export default Sidebar
