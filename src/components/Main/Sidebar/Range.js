import React, { useEffect } from 'react'
import './Range.css'
import Slider from '@material-ui/core/Slider';
import useStyles from '../../../styles/UseStyles';
import { useDispatch, useSelector } from 'react-redux';
import { priceRangeFilter } from '../../../reducers/ProductsReducer/ProductsActions';
import applyFilters from '../../../utils/applyFilters';

const Range = ({title, id, minValue, maxValue, val, rangeValue=[0,0], setRangeValue}) => {
    const classes = useStyles();

    useEffect(() => {
        maxValue === 0  && dispatch(setRangeValue([minValue, maxValue]))
        // eslint-disable-next-line
    }, [minValue, maxValue])

    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const searchValue = useSelector(state => state.products.searchValue);
    const sortValue = useSelector(state => state.products.sortValue);
    
    const handleChangeCommitted = () => {
        if(id === "priceRange") {
            const filteredPriceRange = applyFilters(products, sortValue, searchValue, rangeValue);
            dispatch(priceRangeFilter(filteredPriceRange, rangeValue));
        }
    };

    return (
        <>
            <div className={id}>{title}</div>
            <div className={classes.root}>
                <Slider
                    color='secondary'
                    value={rangeValue}
                    onChange={(event, newValue) => dispatch(setRangeValue(newValue))}
                    onChangeCommitted={handleChangeCommitted}
                    min={minValue}
                    max={maxValue}
                />
            </div>
            <div className="range-values">
                <div className="range-values__item">
                    <div className="value__item value__item--currency">{val}</div>
                    <div className="value__item value__item--price">{rangeValue[0]}</div>
                </div>
                <div className="range-values__item">
                    <div className="value__item value__item--currency">{val}</div>
                    <div className="value__item value__item--price">{rangeValue[1]}</div>
                </div>
            </div>
        </>
    )
}

export default Range;