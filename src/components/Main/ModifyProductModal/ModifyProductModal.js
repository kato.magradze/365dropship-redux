import './ModifyProductModal.css'
import React, { useEffect, useState } from 'react'
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import { InputAdornment } from '@material-ui/core';
import { addProduct, editProduct, getSingleProduct } from '../../../services/productServices';
import { getProducts } from '../../../reducers/ProductsReducer/ProductsActions';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import EditIcon from '@material-ui/icons/Edit';
import noImage from '../../../images/no_image_available.jpg'
import useStyles from '../../../styles/UseStyles';

const productSchema = yup.object({
    title: yup
    .string()
    .required('Required')
    .min(2, 'Title must exceed 2 characters'),
    description: yup
    .string()
    .required('Required')
    .min(4, 'Description must exceed 4 characters'),
    price: yup
    .number()
    .integer('Price must be an integer')
    .required('Required'),
    imageUrl: yup
    .string()
    .url('Input must be a url')
})

const ModifyProductModal = ({ type, id }) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const category = useSelector(state => state.products.category);
    const { enqueueSnackbar } = useSnackbar();

    const classes = useStyles();

    const [image, setImage] = useState("https://i.pinimg.com/originals/3d/6a/a9/3d6aa9082f3c9e285df9970dc7b762ac.gif");

    const formik = useFormik({
        initialValues: {
            title: '',
            description: '',
            price: '',
            imageUrl: ''
        },
        onSubmit: (values) => {
            handleProductSubmit(values);
        },
        validationSchema: productSchema
    });

    useEffect(() => {
        formik.values.imageUrl && setImage(formik.values.imageUrl);
    }, [formik.values.imageUrl])

    useEffect(() => {
        if(id && (type === "edit")) {
            getSingleProduct(id).then(product => {
                formik.setValues(
                    {
                        title: product.title, 
                        description: product.description,
                        price: product.price,
                        imageUrl: product.imageUrl
                    })
            }).catch(err => {
                enqueueSnackbar('Could not set initial values', { variant: 'error' })
            })
        }
        //eslint-disable-next-line
    }, [])

    const handleProductSubmit = (values) => {
        if(type === "edit") {
            editProduct(id, values).then(res => {
                enqueueSnackbar(`Product edited successfully.`, { variant: 'success' })
                history.push(`/catalog/${category}`);
                dispatch(getProducts());
            }).catch(err => {
                enqueueSnackbar(`Could not update product. Try again`, { variant: 'error' })
            })
        }
        else {
            addProduct(values).then(res => {
                enqueueSnackbar(`New product has been added successfully.`, { variant: 'success' })
                history.push(`/catalog/${category}`);
                dispatch(getProducts());
            }).catch(err => {
                enqueueSnackbar(`Could not create new product. Try again`, { variant: 'error' })
            })
        }
    }

    const imgError = (event) => {
        event.target.src = `${noImage}`;
    }

    const handleClose = (event) => {
        if(event.target !== event.currentTarget) return;
        history.push(`/catalog/${category}`);
    }

    return (
        <div className="modal__container" onClick={handleClose}> 
            <div className="modify-product__card">
                <div className="modify-product__header">
                    <h2 className="modify-product__header-text">{type === "edit" ? "Edit Product" : "Add Product"}</h2>
                    <div className="modify-product__header-icon">{type === "edit" ? <EditIcon color="primary"/> : <i className="fas fa-plus"></i>}</div>
                </div>
                {type === "edit" && <div className="modify-product__preview"><img src={image} onError={imgError} alt="" className="modify-product__image"/></div>}
                <form onSubmit={formik.handleSubmit} className="product-form">
                    <TextField
                        label="Product name"
                        variant="outlined"
                        name="title"
                        id="title"
                        value={formik.values.title}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.title && Boolean(formik.errors.title)}
                        helperText={formik.touched.title && formik.errors.title}
                        style={{width: "100%", color: "grey", margin: "12px 0px"}}
                        color="primary"
                    />
                    <TextField
                        label="Description"
                        variant="outlined"
                        name="description"
                        id="description"
                        className={classes.productDescription}
                        multiline
                        rows={5}
                        value={formik.values.description}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.description && Boolean(formik.errors.description)}
                        helperText={formik.touched.description && formik.errors.description}
                        style={{width: "100%", color: "grey", margin: "12px 0px"}}
                        color="primary"
                    />
                    <TextField
                        label="Price"
                        variant="outlined"
                        type="number"
                        name="price"
                        id="price"
                        value={formik.values.price}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.price && Boolean(formik.errors.price)}
                        helperText={formik.touched.price && formik.errors.price}
                        InputProps={{
                            inputProps: { 
                                min: 0 
                            },
                            startAdornment: <InputAdornment position="start">$</InputAdornment>
                        }}
                        style={{width: "100%", color: "grey", margin: "12px 0px"}}
                        color="primary"
                    />
                    <TextField
                        label="Image url"
                        variant="outlined"
                        name="imageUrl"
                        id="imageUrl"
                        value={formik.values.imageUrl}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.imageUrl && Boolean(formik.errors.imageUrl)}
                        helperText={formik.touched.imageUrl && formik.errors.imageUrl}
                        style={{width: "100%", color: "grey", margin: "12px 0px"}}
                        color="primary"
                    />
                    <input type="submit" value="Save" className="authorization__button"/>
                    <input type="button" value="Back" className="authorization__button authorization__button--grey" onClick={handleClose}/>
                </form>
            </div>
        </div>
    )
}

export default ModifyProductModal