import { Button, Drawer } from '@material-ui/core';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { openFilterDrawer, priceRangeFilter, profitRangeFilter, setPriceRange, setProfitRange } from '../../../reducers/ProductsReducer/ProductsActions';
import applyFilters from '../../../utils/applyFilters';
import Choose from '../Sidebar/Choose';
import Range from '../Sidebar/Range';
import ShipOptions from '../Sidebar/ShipOptions';
import './FilterDrawer.css'

const FilterDrawer = () => {

    const filterDrawerOpen = useSelector(state => state.products.filterDrawerOpen);

    const handleDrawerClose = (event) => {
        if(event.target !== event.currentTarget) return;
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        dispatch(openFilterDrawer(false));
    }

    const handleClose = () => {
        dispatch(openFilterDrawer(false));
    }

    const rangeMinMax = useSelector(state => state.products.rangeMinMax);

    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const searchValue = useSelector(state => state.products.searchValue);
    const sortValue = useSelector(state => state.products.sortValue);

    const priceRange = useSelector(state => state.products.rangeStates.priceState);
    const profitRange = useSelector(state => state.products.rangeStates.profitState);

    useEffect(() => {
        dispatch(setPriceRange([rangeMinMax[0], rangeMinMax[1]]))
        dispatch(setProfitRange([0, 100]))
    }, [rangeMinMax, dispatch])
    

    const handleResetFilter = () => {
        rangeMinMax && dispatch(setPriceRange([rangeMinMax[0], rangeMinMax[1]]))
        dispatch(setProfitRange([0, 100]))
        const filteredPriceRange = applyFilters(products, sortValue, searchValue, [rangeMinMax[0], rangeMinMax[1]]);
        dispatch(priceRangeFilter(filteredPriceRange, [rangeMinMax[0], rangeMinMax[1]]));
        dispatch(profitRangeFilter([0, 100]));
    }

    return (
        <Drawer anchor="top" open={filterDrawerOpen} onClose={handleDrawerClose} className="filter-drawer">
            <div
                role="presentation"
                onClick={handleDrawerClose}
                onKeyDown={handleDrawerClose}
            >
                <div className="filter-drawer__container">
                    <Choose title="Choose Niche" modifier="niche" categories={null}/>
                    <Choose title="Choose Category" modifier="category" categories={null}/>
                    <ShipOptions title="Ship From" modifier="shipfrom"/>
                    <ShipOptions title="Ship To" modifier="shipto"/>
                    <ShipOptions title="Select Supplier" modifier="supplier"/>
                    <Range title="Price Range" id="priceRange" minValue={rangeMinMax[0]} maxValue={rangeMinMax[1]} rangeValue={priceRange} setRangeValue={setPriceRange} val="$"/>
                    <Range title="Profit Range" id="profitRange" minValue={0} maxValue={100} rangeValue={profitRange} setRangeValue={setProfitRange} val="%"/>
                    <div className="filter-drawer__button-container">
                        <Button color="primary" variant="contained" onClick={handleResetFilter} style={{width: "100%", color: "white", fontWeight: '600', marginBottom: "15px"}}>Reset filter</Button>
                        <Button variant="contained" onClick={handleClose} style={{width: "100%", color: "white", fontWeight: '600', backgroundColor: "lightgrey"}}>← Back</Button>
                    </div>
                </div>
            </div>
        </Drawer>
    )
}

export default FilterDrawer
