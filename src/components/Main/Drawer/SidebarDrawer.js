import React from 'react'
import './SidebarDrawer.css'
import Drawer from '@material-ui/core/Drawer';
import { Divider, List, ListItem } from '@material-ui/core';
import dropshiplogo from '../../../icons/dropship-logo.png'
import profileimg from '../../../icons/profile-example.jpg'
import { NavLink } from 'react-router-dom'
import icons from '../../../icons/asideIcons';
import { useSelector, useDispatch } from 'react-redux';
import { openSidebarDrawer } from '../../../reducers/ProductsReducer/ProductsActions';

const SidebarDrawer = () => {

    const dispatch = useDispatch();

    const sidebarDrawerOpen = useSelector(state => state.products.sidebarDrawerOpen);

    const handleDrawerClose = (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        dispatch(openSidebarDrawer(false));
    }

    return (
        <Drawer anchor="right" open={sidebarDrawerOpen} onClose={handleDrawerClose} className="drawer">
            <div
                role="presentation"
                onClick={handleDrawerClose}
                onKeyDown={handleDrawerClose}
            >
                <List>
                    <div className="drawer__dropship-logo"><img className="dropship__img" src={dropshiplogo} alt="dropship logo"/></div>
                    <Divider/>
                    <ListItem button key="profile">
                        <NavLink to="/profile" className="drawer-item__container" activeClassName="drawer-item__container--active">
                            <div className="drawer-container__item drawer-container__item--text">Profile</div>
                            <div className="drawer-container__item"><img className="user__img" src={profileimg} alt="user logo" /></div>
                        </NavLink>
                    </ListItem>
                    {icons.map((icon, index) => (
                    <ListItem button key={icon.name}>
                        <NavLink to={`/${icon.path}`} className="drawer-item__container" activeClassName="drawer-item__container--active">
                            <div className="drawer-container__item drawer-container__item--text">{icon.name}</div>
                            <div className="drawer-container__item"><i className={`drawer-icon ${icon.class}`}></i></div>
                        </NavLink>
                    </ListItem>
                    ))}
                </List>
            </div>
        </Drawer>
    )
}

export default SidebarDrawer
