import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router'
import { selectProduct } from '../../../reducers/ProductsReducer/ProductsActions'
import './Catalog.css'
import Header from './Header/Header'
import Products from './Products/Products'
import Sort from './Sort/Sort'

const Catalog = () => {

    const history = useHistory();
    
    const dispatch = useDispatch();

    const [loader, setLoader] = useState(true)

    useEffect(() => {
        const token = localStorage.getItem('token');
        if(!token) {
            history.push("/login");
        }
        setLoader(false);
        //eslint-disable-next-line
    }, [JSON.parse(localStorage.hasOwnProperty('token')), history]);

    useEffect(() => {
        return () => dispatch(selectProduct({}));
    }, [dispatch])

    if(loader) {
        return null
    }
    return (
        <div className="catalog">
            <Header/>
            <Sort/>
            <Products/>
        </div>
    )
}

export default Catalog
