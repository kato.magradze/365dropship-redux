import SingleProduct from "./SingleProduct";
import "./Products.css";
import { Grid } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from "../../../../styles/UseStyles";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useCallback } from "react";
import { getProducts, selectProduct } from "../../../../reducers/ProductsReducer/ProductsActions";
import handleProductCheck from "../../../../utils/handleProductCheck";
import EmptyState from "../EmptyState/EmptyState";
import NoProducts from "../../../../icons/NoProducts";

const Products = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const selectedProducts = useSelector(state => state.products.selectedProducts);
    const filteredProducts = useSelector(state => state.products.filteredProducts);
    const isLoading = useSelector(state => state.products.isLoading);

    useEffect(() => {
        dispatch(getProducts());
    }, [dispatch])

    const handleCheckboxClick = useCallback((id) => {
        const selected = handleProductCheck(selectedProducts, id);
        dispatch(selectProduct(selected));
    }, [dispatch, selectedProducts]) 
    
    return (
        <>
        {isLoading ? 
            (<div className="product__item product__item--image product__item--loading">
                <CircularProgress size={80} color="primary"/>
            </div>)
            :
            filteredProducts.length > 0 
                ? 
                (<Grid className={classes.container} container>
                    {filteredProducts && filteredProducts.map(item => {
                        return (
                            <Grid item xs={12} sm={6} md={4} lg={3} key={item.id}>
                                <SingleProduct
                                    key={item.id}
                                    id={item.id}
                                    isChecked={selectedProducts[item.id] || false}
                                    image={item.imageUrl}
                                    title={item.title}
                                    price={item.price}
                                    description={item.description}
                                    handleCheckboxClick = {handleCheckboxClick}
                                />
                            </Grid>
                        )
                    })}
                </Grid>)
                :
                <EmptyState svg={<NoProducts/>} header="No products to display" text="Oops... looks like there are no products to display for now"/>
        }
        </>
)}

export default Products;