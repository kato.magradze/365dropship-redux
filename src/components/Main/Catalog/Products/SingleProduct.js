import React from 'react'
import './SingleProduct.css'
import { useState } from 'react'
import noImage from '../../../../images/no_image_available.jpg'
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { Box } from '@material-ui/core';
import ProductHover from './ProductHover';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const SingleProduct = ({id, image, title, price, isChecked, handleCheckboxClick }) => {

    const category = useSelector(state => state.products.category);
    const history = useHistory();

    const [src, setSrc] = useState(image)
    const [quantity, setQuantity] = useState(1);
    const [settingsOpen, setSettingsOpen] = useState(null);

    const imgError = () => {
        setSrc(noImage);
    }

    const handleQuantityIncrement = (event) => {
        event.stopPropagation();
        setQuantity(prev => prev + 1);
    }

    const handleQuantityDecrement = (event) => {
        event.stopPropagation();
        setQuantity(prev => {
            if(prev > 0) return prev - 1;
            else return prev;
        });
    }

    const handleOpen = (id) => {
        history.push(`/catalog/${category}/${id}`);
    }

    return (
        <div className={`catalog__item catalog__item--product ${(isChecked || settingsOpen) && "catalog__item--highlighted"}`}>
        <ProductHover id={id} quantity={quantity} setQuantity={setQuantity} isChecked={isChecked} handleCheckboxClick={handleCheckboxClick} settingsOpen={settingsOpen} setSettingsOpen={setSettingsOpen}/>
        <div onClick={() => handleOpen(id)} className="catalog__item--container">
            <div className="catalog__item--container">
                    <div className="product__item product__item--image">
                        <img onError={imgError} className="product__img" alt={`product-${id}`} src={src} />
                    </div>
                    
                    <div className="product__item product__item--title">
                        <h3 className="product__heading">{title}</h3>
                    </div>
                    <div className="product__item product__item--supplier product__item--title">
                        <div className="product__supplier">By: <span className="supplier__name">SP-Supplier115</span></div>
                        <Box p={2}>
                        <ButtonGroup color="primary">
                            <Button onClick={handleQuantityDecrement}>-</Button>
                            <Button disabled style={{color: "grey"}}>{quantity}</Button>
                            <Button onClick={handleQuantityIncrement}>+</Button>
                        </ButtonGroup>
                        </Box>
                    </div>
                <div className="product__item product__item--price">
                    <div className="price__item">
                        <div className="price">${price}</div>
                        <div className="rrp">RRP</div>
                    </div>
                    <div className="divider"></div>
                    <div className="price__item">
                        <div className="price">$6</div>
                        <div className="rrp">Cost</div>
                    </div>
                    <div className="divider"></div>
                    <div className="price__item">
                        <div className="price price__profit">60%/$9</div>
                        <div className="rrp">Profit</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default SingleProduct
