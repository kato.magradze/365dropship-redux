import { addToCart } from '../../../../services/cartServices'
import BlueButton from '../Header/BlueButton'
import './ProductHover.css'
import { useSnackbar } from 'notistack';
import { Button } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { ListItemText } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { removeProduct } from '../../../../services/productServices';
import { getProducts } from '../../../../reducers/ProductsReducer/ProductsActions';

const ProductHover = ({id, isChecked, quantity, setQuantity, handleCheckboxClick, settingsOpen, setSettingsOpen}) => {

    const { enqueueSnackbar } = useSnackbar();
    const history = useHistory();
    const dispatch = useDispatch();

    const category = useSelector(state => state.products.category);
    const isAdmin = useSelector(state => state.common.isAdmin);

    const handleSettingsClick = (event) => {
        setSettingsOpen(event.currentTarget);
    }

    const handleSettingsClose = () => {
        setSettingsOpen(null);
    }

    const handleAddToCart = (id, qty) => {
        setQuantity(1);
        addToCart(id, qty).then(data => {
            enqueueSnackbar(`${qty} product${qty > 1 ? "s have" : " has"} been added to your cart.`, { 
                variant: 'success',
            });
        })
    }

    const handleEdit = () => {
        handleSettingsClose();
        history.push(`/catalog/${category}/${id}/edit`)
    }

    const handleDelete = () => {
        handleSettingsClose();
        removeProduct(id).then(res => {
            enqueueSnackbar('Product has been removed successfully', { variant: "success" })
            dispatch(getProducts());
        }).catch(err => {
            enqueueSnackbar('Could not remove product. Try again', { variant: "error" })
        })
    }

    return (
        <>
            <div className={`product__item product__item--options ${(isChecked || settingsOpen) && "product__item--clicked"}`}>
                <input 
                className="options__item options__item--checkbox"
                id={id} 
                type="checkbox"
                onChange={() => handleCheckboxClick(id)}
                checked={isChecked}
                />
                <label htmlFor={id}></label>
            </div>
            <div className={`product__item product__item--add ${settingsOpen ? "product__item--add-visible" : ""}`}>
                <BlueButton className="options__item options__item--button" title="Add to Inventory" handleClick={() => handleAddToCart(id, quantity)}/>
                {isAdmin &&
                    (<>
                        <Button variant="contained" color="primary" style={{maxWidth: '32px', maxHeight: '32px', minWidth: '32px', minHeight: '32.5px', marginLeft: "-5px", marginBottom: "1px"}} onClick={handleSettingsClick}><i className="fas fa-cog options__item--cog"></i></Button>
                        <Menu
                            anchorEl={settingsOpen}
                            keepMounted
                            open={Boolean(settingsOpen)}
                            onClose={handleSettingsClose}
                        >
                            <MenuItem onClick={handleEdit}>
                                <ListItemIcon>
                                    <EditIcon style={{width: "30px", height: "30px", color: "#61D5DF"}}/>
                                </ListItemIcon>
                                <ListItemText primary="Edit"/>
                            </MenuItem>
                            <MenuItem onClick={handleDelete}>
                                <ListItemIcon>
                                    <DeleteForeverIcon style={{width: "30px", height: "30px", color: "rgba(236, 67, 67, 0.9)"}}/>
                                </ListItemIcon>
                                <ListItemText primary="Remove"/>
                            </MenuItem>
                        </Menu>
                    </>)
                }
            </div>
        </>
    );
}

export default ProductHover;