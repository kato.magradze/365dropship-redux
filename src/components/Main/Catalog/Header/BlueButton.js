import { Button } from '@material-ui/core';
import React from 'react'
import useStyles from '../../../../styles/UseStyles';

const BlueButton = ({title, className, handleClick, big, medium}) => {

    const classes = useStyles();

    return (
        <Button 
            className={
            [classes.button, 
            !(medium || big) ? classes.smallButton : classes.mediumAndBig,
            className ? classes[className] : ""]
            .join(" ")} 
            variant="contained" 
            color="primary" 
            onClick={handleClick} 
            size={big ? 'large' : (medium ? 'medium' : 'small')}>
            {title}
        </Button> 
    );
}

export default BlueButton
