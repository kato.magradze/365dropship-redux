import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import './SelectedProducts.css';

const SelectedProducts = () => {

    const filteredProducts = useSelector(state => state.products.filteredProducts)
    const selectedProducts = useSelector(state => state.products.selectedProducts)

    const reduceSelected = useMemo(() => {
        return Object.values(selectedProducts).reduce((accumulator, currVal) => currVal ? accumulator + 1 : accumulator, 0)
    }, [selectedProducts])

    return (
        <div className="select">
            <div className="select__divider"></div>
            <div className="select__products select__products--visible">{`selected ${reduceSelected} out of ${filteredProducts.length} products`}</div>
            <div className="select__products select__products--shortened">{`${filteredProducts.length} products`}</div>
        </div>
    );
}

export default SelectedProducts;