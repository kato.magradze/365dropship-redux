import React from 'react'
import BlueButton from './BlueButton'
import Hidden from '@material-ui/core/Hidden';
import './Header.css'
import HelpButton from './HelpButton';
import SelectedProducts from './SelectedProducts';
import SearchField from './SearchField';
import { useDispatch, useSelector } from 'react-redux';
import { openFilterDrawer, openSidebarDrawer, selectProduct } from '../../../../reducers/ProductsReducer/ProductsActions';
import { addToCart } from '../../../../services/cartServices';
import { useSnackbar } from 'notistack';
import { Button } from '@material-ui/core';
import { useHistory } from 'react-router';
import useStyles from '../../../../styles/UseStyles';
import MenuIcon from '@material-ui/icons/Menu';

const Header = () => {

    const classes = useStyles();

    const filteredProducts = useSelector(state => state.products.filteredProducts);
    const selectedProducts = useSelector(state => state.products.selectedProducts);
    const category = useSelector(state => state.products.category);
    const isAdmin = useSelector(state => state.common.isAdmin);

    const { enqueueSnackbar } = useSnackbar();

    const dispatch = useDispatch();
    const history = useHistory();

    const handleSelectAll = () => {
        const allSelected = {};
        filteredProducts.forEach(item => {
            allSelected[item.id] = true;
        })
        dispatch(selectProduct(allSelected));
    }

    const handleClearAll = () => {
        dispatch(selectProduct({}));
    }

    const handleAddToCart = async () => {
        const selected = Object.keys(selectedProducts);
        if(selected.length > 0)  {
            selected.reduce((chain, id) => chain.then(() => addToCart(id, 1)), Promise.resolve())
            enqueueSnackbar('Items added to cart successfully.', { variant: 'success' });
        }
        else {
            enqueueSnackbar('Select items to add to cart', { variant: 'info' });
        }
        dispatch(selectProduct({}));
    }

    const handleAddNewProduct = () => {
        history.push(`/catalog/${category}/add`)
    }

    const handleSidebarDrawerOpen = () => {
        dispatch(openSidebarDrawer(true));
    }

    const handleFilterDrawerOpen = () => {
        dispatch(openFilterDrawer(true));
    }

    const handleSelectAllIcon = () => {
        if(Object.keys(selectedProducts).length > 0) {
            handleClearAll();
        } else {
            handleSelectAll();
        }
    }

    return (
        <div className="header__item header__item--functions">
            <div className="functions__item functions__item--select">
                <BlueButton title="Select All" className="selectAllButton" handleClick={handleSelectAll} medium/>
                <SelectedProducts/>
                {Object.keys(selectedProducts).length > 0 && <BlueButton title="Clear Selected" className="clearAllButton" handleClick={handleClearAll} medium/>}
                <Button className={classes.selectAllIcon} color="primary" variant="contained" onClick={handleSelectAllIcon} style={{marginRight: "10px"}}><i className="fas fa-check-circle"></i></Button>
                <Button className={classes.filterIcon} color="primary" variant="contained" onClick={handleFilterDrawerOpen} style={{marginRight: "10px"}}><i className="fas fa-filter"></i></Button>
            </div>
            <div className="functions__item functions__item--search">
                <SearchField/>
                <Hidden xsDown><BlueButton title='Add to Inventory' className="addToInventory" handleClick={handleAddToCart} big/></Hidden>
                <Hidden smUp><BlueButton title='Add' className="add" handleClick={handleAddToCart} big/></Hidden>
                {isAdmin &&
                    <Button className={classes.addProductButton} variant="contained" color="primary" onClick={handleAddNewProduct} style={{ marginLeft: "-2px", marginRight: "4px"}}><i className="fas fa-plus functions__icon--add"></i></Button>
                }
                <MenuIcon color="primary" className={classes.burgerMenu} size="large" onClick={handleSidebarDrawerOpen}/>
                <HelpButton/>
            </div>
        </div>
    )
}

export default Header
