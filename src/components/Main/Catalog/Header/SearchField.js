import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { searchProducts } from '../../../../reducers/ProductsReducer/ProductsActions';
import applyFilters from '../../../../utils/applyFilters';
import './SearchField.css'

const SearchField = () => {

    const [searchValue, setSearchValue] = useState("");

    const products = useSelector(state => state.products.products);
    const sortValue = useSelector(state => state.products.sortValue);
    const priceRangeValue = useSelector(state => state.products.priceRangeValue);
    const dispatch = useDispatch();

    const searchValueChanged = (event) => {
        setSearchValue(event.target.value);
    }

    useEffect(() => {
        const timer = setTimeout(() => {
            const filteredSearch = applyFilters(products, sortValue, searchValue, priceRangeValue);
            dispatch(searchProducts(filteredSearch, searchValue));
        }, 500)
        return () => {
            clearTimeout(timer)
        }
    }, [searchValue, products, sortValue, priceRangeValue, dispatch])

    return (
        <>
            <div className="search__item search__item--field">
                <input 
                className="search__field" 
                type="text" 
                id="searchQuery" 
                placeholder="search..."
                onFocus={(e) => e.target.placeholder = ''}
                value={searchValue}
                onChange={searchValueChanged}
                />
            </div>
            <button className="search__item search__item--button" id="searchButton">
                <i className="fas fa-search"></i>
            </button>
        </>
    );
}

export default SearchField;