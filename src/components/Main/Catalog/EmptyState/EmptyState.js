import React from 'react'
import './EmptyState.css'

const EmptyState = ({ svg, header, text }) => {
    return (
        <div className="empty-state">
            <div className="empty-state__item">{svg}</div>
            <h2 className="empty-state__item empty-state__item--header">{header}</h2>
            <div className="empty-state__item empty-state__item--text">{text}</div>
        </div>
    )
}

export default EmptyState
