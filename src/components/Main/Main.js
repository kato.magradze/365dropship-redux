import React, { useState, useEffect } from 'react'
import './Main.css';
import Catalog from './Catalog/Catalog'
import Sidebar from './Sidebar/Sidebar'
import { useLocation, useParams } from 'react-router';
import Modal from './Modal/Modal';
import ModifyProductModal from './ModifyProductModal/ModifyProductModal';
import { useDispatch, useSelector } from 'react-redux';
import { checkStatus } from '../../reducers/CommonReducer/CommonReducerActions';
import SidebarDrawer from './Drawer/SidebarDrawer';
import FilterDrawer from './Drawer/FilterDrawer';

const Main = () => {

    const location = useLocation();

    const {id, modification} = useParams();
    const category = useSelector(state => state.products.category);
    const isAdmin = useSelector(state => state.common.isAdmin);

    const dispatch = useDispatch();
    const [addProduct, setAddProduct] = useState(false);

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem("user"));
        user && dispatch(checkStatus(user.isAdmin));
    }, [dispatch])

    useEffect(() => {
        if(location.pathname === `/catalog/${category}/add`) {
            setAddProduct(true);
        }
        else {
            setAddProduct(false);
        }
    }, [location, category])

    return (
        <div className="main">
            <Sidebar/>
            <Catalog/>
            {(id && !modification) && 
                <Modal path="catalog"/>
            }
            {(isAdmin && modification) &&
                <ModifyProductModal type={"edit"} id={id}/>
            }
            {(isAdmin && addProduct) &&
                <ModifyProductModal type={"add"} id={id}/>
            }
            <SidebarDrawer/>
            <FilterDrawer/>
        </div>
    )
}

export default Main
