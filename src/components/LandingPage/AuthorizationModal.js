import React, { useEffect, useState } from 'react'
import './AuthorizationModal.css';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import { useHistory } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { checkToken } from '../../utils/checkToken';
import useStyles from '../../styles/UseStyles';

export const AuthorizationModal = ({title, val, buttonText, formik}) => {

    const classes = useStyles();

    const history = useHistory();

    const [showPassword, setShowPassword] = useState(false);
    const [showPasswordConfirmation, setShowPasswordConfirmation] = useState(false);

    useEffect(() => {
        checkToken(history);
    }, [history]);

    const handleFormSubmit = (e) => {
        e.preventDefault()
        formik.handleSubmit()
    }

    return (
        <div className="authorization-modal">
            <div className="authorization__logo"><img src="https://app.365dropship.com/assets/images/auth/logo.svg" alt="logo"/></div>
            <div className="authorization__header">
                <div className="authorization-header__item authorization-header__title">{title}</div>
            </div>
            <form onSubmit={handleFormSubmit} className="authorization__form">
                {val==="register" && (
                        <>
                        <TextField
                        placeholder="First Name"
                        variant="outlined"
                        name="firstName"
                        id="firstName"
                        value={formik.values.firstName}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.firstName && Boolean(formik.errors.firstName)}
                        helperText={formik.touched.firstName && formik.errors.firstName}
                        FormHelperTextProps={{ classes: { root: classes.helperText} }}
                        style={{width: "100%", color: "grey", margin: "15px 0px"}}
                        color="primary"
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <PermIdentityIcon color="primary"/>
                            </InputAdornment>
                        ),
                        }}
                        />
                        <TextField
                        placeholder = "Last Name"
                        name="lastName"
                        id="lastName"
                        variant="outlined"
                        value={formik.values.lastName}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.lastName && Boolean(formik.errors.lastName)}
                        helperText={formik.touched.lastName && formik.errors.lastName}
                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                        style={{width: "100%", color: "grey", margin: "15px 0px"}}
                        color="primary"
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <PermIdentityIcon color="primary"/>
                            </InputAdornment>
                        ),
                        }}
                    />
                    </>
                    )
                }
                <div className="authorization__fields">
                    
                    <TextField
                        placeholder="E-mail"
                        name="email"
                        id="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                        variant="outlined"
                        style={{width: "100%", color: "grey", margin: "15px 0px", outline:"none"}}
                        color="primary"
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <MailOutlineIcon color="primary"/>
                            </InputAdornment>
                        ),
                        }}
                    />
                    <TextField
                        placeholder="Password"
                        name="password"
                        id="password"
                        variant="outlined"
                        type={showPassword ? "text" : "password"}
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.password && Boolean(formik.errors.password)}
                        helperText={formik.touched.password && formik.errors.password}
                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                        style={{width: "100%", color: "grey", margin: "15px 0px"}}
                        color="primary"
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <VpnKeyIcon color="primary"/>
                            </InputAdornment>
                        ),
                        endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() => setShowPassword(!showPassword)}
                                edge="end"
                              >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                        )
                        }}
                    />
                </div>
                {val === "register" && 
                    (
                    <>
                    <TextField
                        placeholder="Confirm Password"
                        name="passwordConfirmation"
                        id="passwordConfirmation"
                        variant="outlined"
                        value={formik.values.passwordConfirmation}
                        type={showPasswordConfirmation ? "text" : "password"}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.passwordConfirmation && Boolean(formik.errors.passwordConfirmation)}
                        helperText={formik.touched.passwordConfirmation && formik.errors.passwordConfirmation}
                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                        style={{width: "100%", color: "grey", margin: "15px 0px"}}
                        color="primary"
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <VpnKeyIcon color="primary"/>
                            </InputAdornment>
                        ),
                        endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() => setShowPasswordConfirmation(prev => !prev)}
                                edge="end"
                              >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                        )
                        }}
                    />
                    <div className="authorization__terms">
                        <div className="authorization-terms__text">
                            <div className="terms-text__wrapper">
                                <div className="terms__text">{"By creating an account, you agree with the"}</div>
                                <div className="terms__text">{"Terms & Conditions and Privacy Policy"}</div>
                            </div>
                            <div className="terms__input">
                                <input type="checkbox" className="terms-input__item terms-input__checkbox"/>
                                <div className="terms-input__item terms-input__text">Subscribe to Newsletter</div>
                            </div>
                        </div>
                        
                    </div>
                    </>
                    )
                }
                {val === "register" &&
                    <div className="login__text">Already have an account? <span className="change-page__button" onClick={() => history.push('/login')}>Log In</span></div>
                }
                {val === "login" && 
                    <div className="login__text">Don't have an account? <span className="change-page__button" onClick={() => history.push('/register')}>Sign Up</span></div>
                }

                <input type="submit" value={buttonText} className="authorization__button"/>
            </form>
        </div>
    )
        
}
