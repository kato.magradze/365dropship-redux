import { Paper } from '@material-ui/core'
import React from 'react'
import './Dashboard.css'

export const Dashboard = () => {
    return (
        <div className="dashboard">
            <div className="dashboard__header">
                <div className="dashboard__header--item">Dashboard</div>
            </div>
            <div className="dashboard__content">
                <Paper elevation={3} style={{ height: 'auto'}}>
                </Paper>
            </div>
        </div>
    )
}