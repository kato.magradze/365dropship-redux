import React, { useEffect } from 'react'
import { addToCart, removeFromCart } from '../../services/cartServices';
import './Cart.css'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import HelpButton from '../Main/Catalog/Header/HelpButton';
import useStyles from '../../styles/UseStyles';
import BlueButton from '../Main/Catalog/Header/BlueButton';
import { useSelector, useDispatch } from 'react-redux';
import { getCartProducts } from '../../reducers/CartReducer/CartActions';
import { useSnackbar } from 'notistack';
import { useHistory, useParams } from 'react-router';
import Modal from '../Main/Modal/Modal';

export const Cart = () => {

    const classes = useStyles();

    const cartProducts = useSelector(state => state.cart.cartProducts);
    // const isLoading = useSelector(state => state.cart.isLoading);

    const { id } = useParams();

    const { enqueueSnackbar } = useSnackbar();

    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(getCartProducts());
    }, [dispatch])

    const handleQuantityChange = (event, id, qty, currentQty) => {
        event.stopPropagation();
        if(qty === -1 && currentQty <= 1) {
            removeItem(id);
        }
        else {
            addToCart(id, qty).then(data => {
                dispatch(getCartProducts())
            }).catch(err => {
                enqueueSnackbar(`Could not change quantity.`, { variant: 'error'});
            })
        }
    }

    const removeItem = (id) => {
        removeFromCart(id).then(res => {
            enqueueSnackbar(`Product has been removed from your cart`, { variant: 'success'});
            dispatch(getCartProducts())
        }).catch(err => {
            enqueueSnackbar(`Could not remove product from your cart`, { variant: 'error'});
        })
    }

    const handleRemoveFromCart = (event, id) => {
        event.stopPropagation();
        removeItem(id);
    }

    const handleTableRowClick = (id) => {
        history.push(`/cart/${id}`);
    }

    return (
            <div className="cart">
                <div className="cart__total">
                    <div className="cart__header--item">{`Shopping cart (${cartProducts.cartItem ? cartProducts.cartItem.items.length : 0})`}</div>
                    <div className="cart__header--item cart__header--right"><HelpButton/></div>
                </div>
                <div className="cart__content-table">
                    <TableContainer component={Paper} className={classes.tableContainer}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left" className={classes.rowHead}>Item Image</TableCell>
                                    <TableCell align="left" className={classes.rowHead}>Description</TableCell>
                                    <TableCell align="center" className={classes.rowHead}>Quantity</TableCell>
                                    <TableCell align="center" className={classes.rowHead}>Item Cost</TableCell>
                                    <TableCell align="center" className={classes.rowHead}>Total Cost</TableCell>
                                    <TableCell align="right" className={classes.rowHead}>{""}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {cartProducts.cartItem && cartProducts.cartItem.items.map((item) => (
                                <TableRow key={item.id} onClick={() => handleTableRowClick(item.id)} className={classes.tableCell}>
                                    <TableCell align="left"><div className="row-image__container"><img src={item.image} alt={item.id} className="row__image"/></div></TableCell>
                                    <TableCell align="left" className={classes.rowTitle}>{item.title}</TableCell>
                                    <TableCell align="center">
                                        <div className="quantity__container">
                                            <button className="quantity__item quantity__button quantity__button--decrement" onClick={(event) => handleQuantityChange(event, item.id, -1, item.qty, item.title)}>-</button>
                                            <div className="quantity__item quantity__item--num">{item.qty}</div>
                                            <button className="quantity__item quantity__button quantity__button--increment" onClick={(event) => handleQuantityChange(event, item.id, 1, item.qty, item.title)}>+</button>
                                        </div>
                                    </TableCell>
                                    <TableCell align="center" className={classes.rowStats}>${item.price}</TableCell>
                                    <TableCell align="center" className={classes.rowStats}>${item.price * item.qty}</TableCell>
                                    <TableCell align="center"><i className="fas fa-trash remove-item__icon" onClick={(event) => handleRemoveFromCart(event, item.id)}></i></TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>

                    <div className="cart-stats__container">
                        <TableContainer component={Paper} className={classes.tableContainer}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="left"><BlueButton title="← Continue shopping" big/></TableCell>
                                        <TableCell align="center">
                                            <div className="cart__balance">Balance</div>
                                            <div className="cart__balance--bucks">$0.00</div>
                                        </TableCell>
                                        <TableCell align="center">
                                            <div className="cart__balance">Items Total</div>
                                            <div className="cart__balance--bucks">${cartProducts.cartItem ? cartProducts.cartItem.totalAmount : "0.00"}</div>
                                        </TableCell>
                                        <TableCell align="center">
                                            <div className="cart__balance">Shipping total</div>
                                            <div className="cart__balance--bucks">$0.00</div>
                                        </TableCell>
                                        <TableCell align="center">
                                            <div className="cart__balance">Order total</div>
                                            <div className="cart__balance--bucks">${cartProducts.cartItem ? cartProducts.cartItem.totalAmount : "0.00"}</div>
                                        </TableCell>
                                        <TableCell align="right"><BlueButton title="Checkout" big/></TableCell>
                                    </TableRow>
                                </TableHead>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
                {id &&
                    <Modal path="cart"/>
                }
            </div>
    )
}