import React, { useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { setSnackbar } from "../../reducers/CommonReducer/CommonReducerActions";

const CustomizedSnackbars = () => {
  const dispatch = useDispatch();

  const snackbarState = useSelector(state => state.common.snackbarState);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    dispatch(setSnackbar(false, snackbarState.type, snackbarState.message));
  };

  const vertical = useMemo(() => "bottom", [])
  const horizontal = useMemo(() => "left", [])

  return (
    <div className={""}>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={snackbarState.open}
        autoHideDuration={2500}
        onClose={handleClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleClose}
          color={snackbarState.type}
          severity={snackbarState.type}
        >
          {snackbarState.message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default CustomizedSnackbars;