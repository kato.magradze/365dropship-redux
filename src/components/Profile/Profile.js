import { Button, Paper, TextField } from '@material-ui/core'
import React, { useEffect } from 'react'
import useStyles from '../../styles/UseStyles'
import HelpButton from '../Main/Catalog/Header/HelpButton'
import './Profile.css'
import profileimg from '../../icons/profile-example.jpg'
import { useHistory } from 'react-router'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { checkPassword, getUser, updateUser } from '../../services/profileServices'
import { useSnackbar } from 'notistack'

const profileSchema = yup.object().shape({
    firstName: yup
    .string()
    .required('Required'),
    lastName: yup
    .string()
    .required('Required'),
    email: yup
    .string()
    .email('Invalid email address')
    .required('Required'),
    currentPassword: yup
    .string()
    .when("newPassword", {
        is: (newPassword) => !!newPassword,
        then: yup.string().required("Required"),
        otherwise: yup.string().notRequired(),
    }),
    newPassword: yup
    .string()
    .when("currentPassword", {
        is: (currentPassword) => !!currentPassword,
        then: yup.string().required("Required").min(4, 'Password must be at least 4 characters long'),
        otherwise: yup.string().notRequired(),
    }),
    confirm: yup
    .string()
    .when("currentPassword", {
        is: (currentPassword) => !!currentPassword,
        then: yup.string().required("Required").oneOf([yup.ref('newPassword'), null], 'Passwords do not match'),
        otherwise: yup.string().notRequired(),
    }),
}, [['currentPassword', 'newPassword']])

export const Profile = () => {

    const classes = useStyles();

    const { enqueueSnackbar } = useSnackbar();

    const history = useHistory();

    const handleSignOut = () => {
        localStorage.clear();
        history.push('/login');
    }

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            currentPassword: '',
            newPassword: '',
            confirm: ''
        },
        onSubmit: (values) => {
            const user = JSON.parse(localStorage.getItem('user'))
            values.newPassword ? 
            (checkPassword(user.email, values.currentPassword).then(result => {
                handleUpdateUser(user.id, values.firstName, values.lastName, values.email, values.newPassword);
            }).catch(err => {
                enqueueSnackbar('Current email or password is incorrect', { variant: 'error' })
            }))
            :
            handleUpdateUser(user.id, values.firstName, values.lastName, values.email)
        },
        validationSchema: profileSchema,
    });

    const getUserFields = () => {
        const user = JSON.parse(localStorage.getItem('user'))
        getUser(user.id).then(data => {
            formik.setValues(
                {
                    firstName: data.firstName, 
                    lastName: data.lastName,
                    email: data.email,
                })
        }).catch(err => {
            enqueueSnackbar('Could not set initial values', { variant: 'error' })
        })
    }

    const handleUpdateUser = (id, firstName, lastName, email, password) => {
        updateUser(id, firstName, lastName, email, password).then(result => {
            localStorage.setItem("user", JSON.stringify(result));
            enqueueSnackbar('User resource was updated successfully', { variant: 'success' });
        }).catch(err => {
            enqueueSnackbar('Could not update user resource, try again', { variant: 'error' });
        }).finally(() => {
            getUserFields();
        })
    }

    useEffect(() => {
        getUserFields();
        //eslint-disable-next-line
    }, [])

    return (
        <div className="profile">
            <div className="profile__header">
                <div className="profile__header--item">My Profile</div>
                <div className="profile__header--item">
                    <Button variant="contained" onClick={handleSignOut} style={{color: "#4A87EF", backgroundColor: "#E6EEFC", fontWeight: "600", width: "150px", textTransform: "uppercase", fontSize: '16px', marginRight: '30px'}}>Sign Out</Button>
                    <HelpButton/>
                </div>
            </div>
            <div className="profile__content">
                <Paper elevation={3} style={{ height: 'auto'}}>
                    <div className="profile-content__header">
                        <div className="profile-content__header-item profile-content__header-item--billing">
                            <div className="content-header__item content-header__item--profile">Profile</div>
                            <div className="content-header__item">Billing</div>
                            <div className="content-header__item">Invoice history</div>
                        </div>
                        <div className="profile-content__header-item">
                            <div className="profile-content__deactivate"><Button className={classes.deactivateButton} variant="contained">Deactivate Account</Button></div>
                        </div>
                    </div>
                    <form className="container" onSubmit={formik.handleSubmit}>
                        <div className="profile-info__container">
                            <div className="profile-info__box">
                                <div className="profile-info__item profile-info__item--title">Profile Picture</div>
                                <div className="profile-info__item profile-info__item--content">
                                    <div className="profile-image"><img src={profileimg} alt="profile img"/></div>
                                    <div className="profile-upload"><Button color="primary" variant="contained" style={{color: "white", fontWeight: "600", width: "150px", textTransform: "capitalize", fontSize: '16px'}}>Upload</Button></div>
                                </div>
                            </div>
                            <div className="profile-info__box">
                                <div className="profile-info__item profile-info__item--title">Personal Details</div>
                                <div className="profile-info__item profile-info__item--content">
                                    <TextField
                                        placeholder="First Name"
                                        variant="outlined"
                                        id="firstName"
                                        name="firstName"
                                        className={classes.profileField}
                                        value={formik.values.firstName}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.firstName && Boolean(formik.errors.firstName)}
                                        helperText={formik.touched.firstName && formik.errors.firstName}
                                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                                    />
                                    <TextField
                                        placeholder="Last Name"
                                        variant="outlined"
                                        id="lastName"
                                        name="lastName"
                                        className={classes.profileField}
                                        value={formik.values.lastName}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.lastName && Boolean(formik.errors.lastName)}
                                        helperText={formik.touched.lastName && formik.errors.lastName}
                                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                                    />
                                    <TextField
                                        placeholder="Country"
                                        variant="outlined"
                                        className={classes.profileField}
                                    />
                                </div>
                            </div>
                            <div className="profile-info__box">
                                <div className="profile-info__item profile-info__item--title">Change Password</div>
                                <div className="profile-info__item profile-info__item--content">
                                    <TextField
                                        placeholder="Current Password"
                                        variant="outlined"
                                        type="password"
                                        className={classes.profileField}
                                        id="currentPassword"
                                        name="currentPassword"
                                        value={formik.values.currentPassword}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.currentPassword && Boolean(formik.errors.currentPassword)}
                                        helperText={formik.touched.currentPassword && formik.errors.currentPassword}
                                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                                    />
                                    <TextField
                                        placeholder="New Password"
                                        variant="outlined"
                                        type="password"
                                        className={classes.profileField}
                                        id="newPassword"
                                        name="newPassword"
                                        value={formik.values.newPassword}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.newPassword && Boolean(formik.errors.newPassword)}
                                        helperText={formik.touched.newPassword && formik.errors.newPassword}
                                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                                    />
                                    <TextField
                                        placeholder="Confirm"
                                        variant="outlined"
                                        type="password"
                                        className={classes.profileField}
                                        id="confirm"
                                        name="confirm"
                                        value={formik.values.confirm}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.confirm && Boolean(formik.errors.confirm)}
                                        helperText={formik.touched.confirm && formik.errors.confirm}
                                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                                    />
                                </div>
                            </div>
                            <div className="profile-info__box">
                                <div className="profile-info__item profile-info__item--title">Contact Information</div>
                                <div className="profile-info__item profile-info__item--content">
                                    <TextField
                                        placeholder="Email"
                                        variant="outlined"
                                        id="email"
                                        name="email"
                                        className={classes.profileField}
                                        value={formik.values.email}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.email && Boolean(formik.errors.email)}
                                        helperText={formik.touched.email && formik.errors.email}
                                        FormHelperTextProps={{ classes: { root: classes.helperText } }}
                                    />
                                    <TextField
                                        placeholder="Skype"
                                        variant="outlined"
                                        className={classes.profileField}
                                    />
                                    <TextField
                                        placeholder="Phone"
                                        variant="outlined"
                                        className={classes.profileField}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="profile__save-changes"><Button type="submit" color="primary" variant="contained" style={{color: "white", fontWeight: "600", width: "150px", textTransform: "capitalize", fontSize: '16px'}}>Save Changes</Button></div>
                    </form>
                </Paper>
            </div>
        </div>
    )
}