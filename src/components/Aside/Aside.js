import './Aside.css';
import dropshiplogo from '../../icons/dropship-logo.png';
import profileimg from '../../icons/profile-example.jpg';
import { NavLink } from 'react-router-dom'
import icons from '../../icons/asideIcons';

const Aside = () => {

    return (
        <aside className="aside">
            <section className="aside__item aside__item--nav">
                <div className="aside__icon aside__icon--dropship">
                    <img className="dropship__img" src={dropshiplogo} alt="dropship logo" />
                </div>
                <NavLink to="/profile">
                <div className="aside__icon aside__icon--user">
                    <img className="user__img" src={profileimg} alt="user logo" />
                </div>
                </NavLink>

                {icons.map(item => {
                    return <div className={`aside__icon aside__icon--${item.path}`} key={item.name}><NavLink to={`/${item.path}`} activeClassName='aside__icon--active' className="aside__navlink"><i className={`icon ${item.class}`}></i></NavLink></div>
                })}
            </section>
        </aside>
    );
}

export default Aside;