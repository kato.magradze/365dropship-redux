import { combineReducers } from "redux";
import CartReducer from "./CartReducer/CartReducer";
import CommonReducer from "./CommonReducer/CommonReducer";
import ProductsReducer from "./ProductsReducer/ProductsReducer";

export const rootReducer = combineReducers({
    products: ProductsReducer,
    cart: CartReducer,
    common: CommonReducer
})