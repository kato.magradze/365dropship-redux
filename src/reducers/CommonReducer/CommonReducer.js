import * as types from './CommonReducerActionTypes'

const initialState = {
    isAdmin: false,
    snackbarState: {
        open: false,
        type: "success",
        message: ""
    }
}

const CommonReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.CHECK_STATUS: 
            return {
                ...state,
                isAdmin: action.payload.isAdmin
            }
        case types.SET_SNACKBAR: 
        return {
            ...state,
            snackbarState: {
                open: action.payload.snackbarOpen,
                type: action.payload.snackbarType,
                message: action.payload.snackbarMessage
            }
        }
        default: 
            return {...state}
    }
}

export default CommonReducer