import * as types from './CommonReducerActionTypes'

export const checkStatus = (isAdmin) => {
    return {
        type: types.CHECK_STATUS,
        payload: { isAdmin }
    }
}

export const setSnackbar = (snackbarOpen, snackbarType, snackbarMessage) => {
    return {
        type: types.SET_SNACKBAR,
        payload: { snackbarOpen, snackbarType, snackbarMessage }
    }
}