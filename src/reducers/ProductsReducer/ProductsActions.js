import { getAllProductsList } from '../../services/productServices'
import handleMaxPrice from '../../utils/handleMaxPrice'
import { setSnackbar } from '../CommonReducer/CommonReducerActions'
import * as types from './ProductsActionTypes'

export const getProducts = () => async (dispatch) => {

    dispatch({
        type: types.LOADING,
        payload: { isLoading: true }
    })

    try {
        const productsData = await getAllProductsList();
        const rangeMinMax = handleMaxPrice(productsData);

        dispatch({
            type: types.GET_PRODUCTS,
            payload: {
                products: productsData,
                priceRange: rangeMinMax
            }
        })
    } catch(err) {
        dispatch(setSnackbar(true, "error", "Server error. Try again"));
    } finally {
        dispatch({
            type: types.LOADING,
            payload: { isLoading: false }
        })
    }
}

export const sortProducts = (filteredProducts, sortValue) => {
    return {
        type: types.SORT_PRODUCTS,
        payload: { filteredProducts, sortValue }
    }
}

export const searchProducts = (filteredProducts, searchValue) => {
    return {
        type: types.SEARCH_PRODUCTS,
        payload: { filteredProducts, searchValue }
    }
}

export const priceRangeFilter = (filteredProducts, priceRangeValue) => {
    return {
        type: types.PRICE_RANGE,
        payload: { filteredProducts, priceRangeValue }
    }
}

export const profitRangeFilter = (profitRangeValue) => {
    return {
        type: types.PROFIT_RANGE,
        payload: { profitRangeValue }
    }
}

export const selectProduct = (selectedProducts) => {
    return {
        type: types.SELECT_PRODUCT,
        payload: { selectedProducts }
    }
}

export const openSidebarDrawer = (drawerOpen) => {
    return {
        type: types.OPEN_SIDEBAR_DRAWER,
        payload: { drawerOpen }
    }
}

export const openFilterDrawer = (drawerOpen) => {
    return {
        type: types.OPEN_FILTER_DRAWER,
        payload: { drawerOpen }
    }
}

export const setPriceRange = (priceState) => {
    return {
        type: types.SET_PRICE_STATE,
        payload: { priceState }
    }
}

export const setProfitRange = (profitState) => {
    return {
        type: types.SET_PROFIT_STATE,
        payload: { profitState }
    }
}