import * as types from './ProductsActionTypes'

const initialState = {
    products: [],
    filteredProducts: [],
    isLoading: true,
    searchValue: "",
    sortValue: "default",
    selectedProducts: {},
    rangeMinMax: [0, 0],
    // rangeMinMax: null,
    priceRangeValue: [0, 0],
    profitRangeValue: [0, 100],
    category: "all-categories",
    sidebarDrawerOpen: false,
    filterDrawerOpen: false,
    rangeStates: {
        priceState: [0, 0],
        profitState: [0, 100]
    }
}

const ProductsReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.GET_PRODUCTS:
            return {
                ...state, 
                products: action.payload.products,
                filteredProducts: action.payload.products,
                priceRangeValue: action.payload.priceRange,
                rangeMinMax: action.payload.priceRange
            }
        case types.SORT_PRODUCTS: 
            return {
                ...state,
                filteredProducts: action.payload.filteredProducts,
                sortValue: action.payload.sortValue
            }
        case types.SEARCH_PRODUCTS:
            return {
                ...state,
                filteredProducts: action.payload.filteredProducts,
                searchValue: action.payload.searchValue
            }
        case types.PRICE_RANGE:
            return {
                ...state,
                filteredProducts: action.payload.filteredProducts,
                priceRangeValue: action.payload.priceRangeValue
            }
        case types.PROFIT_RANGE: {
            return {
                ...state,
                profitRangeValue: action.payload.profitRangeValue
            }
        }
        case types.SELECT_PRODUCT: {
            return {
                ...state,
                selectedProducts: action.payload.selectedProducts
            }
        }
        case types.OPEN_SIDEBAR_DRAWER: {
            return {
                ...state, 
                sidebarDrawerOpen: action.payload.drawerOpen
            }
        }
        case types.OPEN_FILTER_DRAWER: {
            return {
                ...state, 
                filterDrawerOpen: action.payload.drawerOpen
            }
        }
        case types.SET_PRICE_STATE: {
            return {
                ...state,
                rangeStates: {
                    ...state.rangeStates,
                    priceState: action.payload.priceState,
                }
            }
        }
        case types.SET_PROFIT_STATE: {
            return {
                ...state,
                rangeStates: {
                    ...state.rangeStates,
                    profitState: action.payload.profitState,
                }
            }
        }
        case types.LOADING:
            return {...state, isLoading: action.payload.isLoading}
        default: 
            return {...state}
    }
}

export default ProductsReducer