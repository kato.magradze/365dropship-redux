import * as types from './CartActionTypes'
import { getCart } from '../../services/cartServices';
import { setSnackbar } from '../CommonReducer/CommonReducerActions';

export const getCartProducts = () => async (dispatch) => {

    dispatch({
        type: types.LOADING_CART
    })

    try {
        const cartData = await getCart();

        dispatch({
            type: types.GET_CART_PRODUCTS,
            payload: {
                cartProducts: cartData
            }
        })
    } catch(err) {
        dispatch(setSnackbar(true, "info", "Empty cart."))
    }
}