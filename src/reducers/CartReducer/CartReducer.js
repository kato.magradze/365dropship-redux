import * as types from './CartActionTypes'

const initialState = {
    cartProducts: [],
    isLoading: true
}

const CartReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.GET_CART_PRODUCTS: 
            return {
                ...state,
                cartProducts: action.payload.cartProducts,
                isLoading: false
            }
        case types.LOADING_CART:
            return {...state, isLoading: true}
        default: 
            return {...state}
    }
}

export default CartReducer